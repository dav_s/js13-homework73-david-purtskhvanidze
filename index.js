const express = require('express');
const vigenere = require('caesar-salad').Vigenere;
const app = express();
const port = 8000;
const pass = 'password';

app.get('/', (req, res) => {
    res.send('Hello, world!\n');
});

app.get('/:echo', (req, res) => {
    res.send(req.params.echo);
});

app.get('/encode/:text', (req, res) => {
    res.send(vigenere.Cipher(pass).crypt(req.params.text));
});

app.get('/decode/:text', (req, res) => {
    res.send(vigenere.Decipher(pass).crypt(req.params.text));
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});